# SLM project

![release](https://img.shields.io/badge/release-1.0.0-blue)

---

## Summary

* [Consignes](#consignes)
* [Credit](#credit)

---

## Consignes

Gérer des transactions entre utilisateur d'une banque.

- [X] Créer les entités (user ...)
- [X] Gestion des transactions entre utilisateur

---

## Credit

![Paul Hoareau](https://img.shields.io/badge/Paul-Hoareau-red?style=for-the-badge&logo=superuser)
![Tony Le Fur Joguet](https://img.shields.io/badge/Tony-Le%20Fur%20Joguet-red?style=for-the-badge&logo=superuser)
![Pierre Saugues](https://img.shields.io/badge/Pierre-Saugues-red?style=for-the-badge&logo=superuser)

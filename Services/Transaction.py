# Pierre's code
from Decorators.save_in_file_decorator import decode_save_in_file_decorator


class Transaction:
    # save the result function in the file ./Logs/log.txt
    @staticmethod
    @decode_save_in_file_decorator("./Logs/log.txt")
    def user_to_user_transaction(sender, receiver, amount):
        try:
            # check if the sold is greater than the amount to spend
            if sender.get_balance() > amount:
                sender.debit(amount)
            else:
                # if not write it inside the log file
                return sender.get_name() + " dont have enough money"
        except Exception as e:
            # if an error arise raise it
            raise Exception("Votre debit à echoué.")

        try:
            # check if the credit goes well
            receiver.credit(amount)
        except Exception as e:
            # if an error arise raise it
            raise Exception("Erreur lors de la créditation de votre transaction.")

        # prepare the message to log
        return receiver.get_name() + " has received " + str(amount) + " from " + sender.get_name()

import uuid
from Decorators.NameValidation import name_validation_decorator


# Tony LE FUR - - JOGUET
class Person:
    @name_validation_decorator
    def __init__(self, name):
        self._id = str(uuid.uuid4())
        self._name = name

    def get_id(self):
        return self._id

    def get_name(self):
        return self._name

    def set_name(self, value):
        self._name = value

    def get_email(self):
        return self._email

    def set_email(self, value):
        self._email = value

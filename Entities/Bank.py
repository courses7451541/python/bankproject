from Services.Transaction import Transaction
from Decorators.VerifyUser import verify_user
class Bank:
    def __init__(self):
        self.__list_clients = []

    # Paul HOAREAU
    @verify_user
    def add_client(self, user):
        self.__list_clients.append(user)

    # Paul HOAREAU
    def get_all_clients(self):
        return self.__list_clients

    # Paul HOAREAU
    def del_client(self, user):
        self.__list_clients.remove(user)

    # Paul HOAREAU
    def pay(self, user_one, user_two, amount):
        if user_one not in self.__list_clients or user_two not in self.__list_clients:
            raise ValueError("Client does not exist in our bank")
        if user_one.get_balance() < amount:
            raise ValueError("You do not have enough")
        transaction = Transaction()
        response = transaction.user_to_user_transaction(user_one, user_two,amount)
        print(response)


import random

from Entities.Person import Person


# Tony LE FUR - - JOGUET
class User(Person):
    def __init__(self, name):
        super().__init__(name)
        self._balance = random.randint(1, 100000)

    def get_balance(self):
        return self._balance

    def credit(self, value):
        if value > 0:
            self._balance += value
        else:
            raise ValueError("Value must be greater than 0.")

    def debit(self, value):
        if value < self._balance:
            self._balance -= value
        else:
            raise ValueError("You cannot credit")

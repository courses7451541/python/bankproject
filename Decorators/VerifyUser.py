def verify_user(func):
    def wrapper(*args, **kwargs):
        if args[1] in args[0].get_all_clients():
            raise ValueError("Client does not exist")
        func(*args, **kwargs)
    return wrapper
# Tony LE FUR - - JOGUET
def name_validation_decorator(func):
    def wrapper(self, *args, **kwargs):
        print(args[0])
        if not isinstance(args[0], str):
            raise ValueError("Name must be a string")
        func(self, *args, **kwargs)

    return wrapper

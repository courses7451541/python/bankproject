# Pierre's code
# create a parameter for the file name
def decode_save_in_file_decorator(file_name_to_write):
    def save_in_file_decorator(func):
        def save_in_file_decorator_wrapper(self, *args, **kwargs):
            # save result for log
            result = func(self, *args, **kwargs)
            # write in the file pass in parameter
            try:
                with open(file_name_to_write, "a") as file:
                    file.write(f"{result}.\n")
            except Exception as e:
                # if an error arise raise it
                raise Exception("Log not created.")

            return result

        return save_in_file_decorator_wrapper

    return save_in_file_decorator
